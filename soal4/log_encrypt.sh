#! /bin/bash

# Setting Up Cron
# Set user's home cron to command path
    cd /home/zeonkun/SISOP/praktikum1/sisop-praktikum-modul-1-2023-am-c05/soal4
# Creates a new crontab
#   crontab -e
# Insert the command and the shell
#   0 */2 * * * /bin/bash /home/zeonkun/SISOP/praktikum1/sisop-praktikum-modul-1-2023-am-c05/soal4/log

# Setting up the SYSLOG path on logPath variable
logPath="/var/log/syslog"

# Alphabets that will be encrypted or ciphered
cypherArtifacts="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

# Encrypt Function, further used in tr command
function encrypt () {
    # Local declaration
    local agentOfEncryption=$1

    # BRUTE FORCE -- DELETE AFTER 10:00 AM
    agentOfEncryption=$(echo $agentOfEncryption | cut -c2)
    
    local temp=""

    # Separates Upper and Lower cases
    upperBound="$(echo ${cypherArtifacts:26:51})"
    lowerBound="$(echo ${cypherArtifacts:0:26})"

    # Get the range for each case using the substring concept {string:startingArtifact:listOfArtifacts}
    temp+="${lowerBound:agentOfEncryption%26:26}${lowerBound:0:agentOfEncryption%26}"
    temp+="${upperBound:agentOfEncryption%26:26}${upperBound:0:agentOfEncryption%26}"
    echo $temp
}

# Implement encrypt function on logPath inner text
function ThyFullEncryptionPack () {
    local filename="$(date +"%H:%M %d:%m:%Y").txt"
    echo -n "$(cat $logPath | tr [$(echo $cypherArtifacts)] [$(encrypt $(date +"%H"))] >> "$filename")"
}

# Fully Encrypt the SYSLOG & Backup the data to a txt file (with TIME FORMAT)
echo -n "$(ThyFullEncryptionPack)"