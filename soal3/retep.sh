#! /bin/bash
echo "Selamat datang User"
read -p "Username: " User
user_info=$(grep "^$User:" ./users/users.txt)
if [[ -n "$user_info" ]]; then
  password=$(echo "$user_info" | cut -d ":" -f 2)
else
  echo "Invalid username or password"
  echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $User" >> log.txt
  exit 1
fi
read  -p "Password: " input_password
echo
if [[ "$input_password" == "$password" ]]; then
  echo "SUKSES!"
  echo "$(date +'%y/%m/%d %H:%M:%S')  $User telah berhasil login" >> log.txt
else
  echo "Invalid username or password"
  echo "$(date +'%y/%m/%d %H:%M:%S')  $User telah gagal login" >> log.txt
  exit 1
fi
