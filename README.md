# sisop-praktikum-modul-1-2023-AM-C05

## Soal 1
Di dalam university_survey.sh, terdapat beberapa perintah yang merupakan jawaban dari tiap poin yang ada di soal 1.  

Pada poin 1a, maka akan menjalankan script yang memfilter kode 'JP' dari asal negara universitas yang ada pada csv. Kemudian akan dilakukan pengambilan untuk 5 line teratas menggunakan **sed**, dan di diambil nama universitasnya saja menggunakan **cut**
```
echo "Top 5 Universities in Japan"
awk '/JP/' "2023 QS World University Rankings.csv" | sed -n 1,5p | cut -d',' -f2
```
dengan hasil sebagai berikut
```
Top 5 Universities in Japan :
The University of Tokyo
Kyoto University
Tokyo Institute of Technology (Tokyo Tech)
Osaka University
Tohoku University
```

Pada poin 1b, script yang dijalankan adalah berikut, dengan melakukan filter untuk baris dengan kode 'JP'. Kemudian hasil filter tersebut akan disort berdasarkan kolom FSR secara ascending, yang dimana nanti akan diambil nama univnya saja.
```
echo "Lowest 5 FSR score Universities in Japan :"
awk '/JP/' "2023 QS World University Rankings.csv" | sort -t, -k9 -nr | tail -n 5 | sort -t, -k9 -n | cut -d',' -f2
```
dengan hasil
```
Lowest 5 FSR score Universities in Japan :
Ritsumeikan Asia Pacific University
Shibaura Institute of Technology
Kwansei Gakuin University
Doshisha University
Meiji University
```
Poin 1c, menjalankan script yang akan memfilter univ dengan kode 'JP', kemudian di sort dengan kolom GER, dan diambil top 10.
```
echo "Top 10 Universities in Japan sorted by GER ranking :"
awk '/JP/' "2023 QS World University Rankings.csv" | sort -t, -k20 -n | sed -n 1,10p | cut -d',' -f2
```
dengan hasil
```
Top 10 Universities in Japan sorted by GER ranking :
The University of Tokyo
Keio University
Waseda University
Hitotsubashi University
Tokyo University of Science
Kyoto University
Nagoya University
Tokyo Institute of Technology (Tokyo Tech)
International Christian University
Kyushu University
```
Dan terakhir poin 1d, menjalankan script yang memfilter kampus dengan kata kunci "Keren"
```
echo "The Coolyeah :"
awk '/Keren/' "2023 QS World University Rankings.csv" | cut -d',' -f2
```
dengan hasil
```
The Coolyeah :
Institut Teknologi Sepuluh Nopember (ITS Surabaya Keren)
```
**Kendala :** agak bingung dengan keyword yang digunakan pada setiap command seperti -k , -nr, -f dkk saat pertama kali mengerjakan. 

## Soal 2
Ketika program dieksekusi, program akan mengambil tanggal lalu menyimpannya ke variabel latestday. Variabel firstfolder dan lastfolder digunakan sebagai batas folder mana saja yang akan dizip. Pada perulangan while baris 7, diambil jam untuk menentukan berapa banyak gambar yang perlu diunduh dan disimpan di variabel hour, lalu diunduhlah gambar sebanyak "hour" kali. Selanjutnya, untuk menentukan apakah perlu dilakukan zip, diambil tanggal sekarang lalu disimpan di variabel currentday. Apabila currentday berbeda dari latestday, hal tersebut menandakan bahwa telah terjadi pergantian hari sehingga perlu dilakukan zip. Zip dilakukan dari firstfolder hingga lastfolder. Nilai lastfolder selalu bertambah pada setiap pengulangan while baris 7, sedangkan firstfolder bertambah menjadi lastfolder+1 setelah dilakukan zip. Latestday juga diubah sesuai dengan currentday setelah dilakukan zip. Command sleep pada baris 33 digunakan untuk menunda pengulangan selama 10 jam agar pengunduhan gambar berlangsung setiap 10 jam. Berikut screenshoot setelah program berjalan.
![](images/2-1.png)
![](images/2-2.png)
![](images/2-3.png)
![](images/2-4.png)

**Kendala :** Tidak ada kendala. Hanya sempat terkena sidang karena repo open public dan dicopas teman seangkatan.
## Soal 3

Pada soal ketiga kita akan membuat sebuah sistem regris dan login sederhana menggunakan shell scripting. Untuk script regristasi diberi nama file "louis.sh" sedangkan untuk login diberi nama "retep.sh". Pada script regris dibutuhkan folder **users** dengan sebuah file **users.txt** untuk menyimpan data regristasi user. Kemudian pada script login kita membutuhkan file **log.txt** untuk menyimpan log yang terjadi pada proses aplikasi. 

Berikut adalah script untuk mendaftar sebagai user.

```
#!/bin/bash

echo "Welcome New User Please Register"
read -p "Username: " username

if grep -q "^$username:" ./users/users.txt; then
	echo "Username sudah terdaftar."
	echo "$(date +'%y/%m/%d %H:%M:%S') Gagal Mendaftar Username sudah ada" >> log.txt
	exit 1
fi
while true; do
	read -p "Password: " password
	echo
  
  if [[ ${#password} -ge 8 && "$password" =~ [A-Z] && "$password" =~ [a-z] && "$password" != "$username" && "$password" =~ [0-9]  ]]; then
    echo Pengecekan 1 lulus.

        if [[$password =~ chicken && $password =~ ernie]]; then
          echo "Password tidak memenuhi syarat $username harap registrasi kembali"
        else
          break
        fi

  else
    echo "Password tidak memenuhi syarat $username harap registrasi kembali"
  fi
done

echo "$username:$password" >> ./users/users.txt
echo "User berhasil registrasi"
echo "$(date + '%y/%m/%d %H:%M:%S') $username berhasil registrasi" >> log.txt
```
Script berikut berguna dalam memasukan user yang sudah memenuhi ketentuan username maupun password kedalam users.txt serta mengecek apabila adanya username yang sudah terdaftar namun digunakan kembali.

Selanjutnya adalah script untuk login.

```
#!/bin/bash

echo "Selamat datang User"

read -p "Username: " User

user_info=$(grep "^$User:" ./users/users.txt)

if [[ -n "$user_info" ]]; then
  password=$(echo "$user_info" | cut -d ":" -f 2)
else
  echo "Invalid username or password"
  echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $User" >> log.txt
  exit 1
fi

read  -p "Password: " input_password
echo

if [[ "$input_password" == "$password" ]]; then
  echo "SUKSES!"
  echo "$(date +'%y/%m/%d %H:%M:%S')  $User telah berhasil login" >> log.txt
else
  echo "Invalid username or password"
  echo "$(date +'%y/%m/%d %H:%M:%S')  $User telah gagal login" >> log.txt
  exit 1
fi

```
Berikut adalah salah satu contoh regristasi dan login, beserta isi file users.txt dan log.txt. 

Login
```
Welcome New User Please Register
Username: Zeonist
Password: ABCabc123

Pengecekan 1 lulus.
./louis.sh: line 18: [[ABCabc123: command not found
User berhasil registrasi
```

Regristasi
```
Selamat datang User
Username: Zeonist
Password: ABCabc123

SUKSES!
```

users.txt
```
Zeonist:ABCabc123
```

log.txt
```
23/03/10 15:25:44 REGISTER: INFO User Zeonist registered successfully
23/03/10 15:26:39  Zeonist telah berhasil login
```

**Kendala :** Sempat terevisi, karena echo yang ditulis kurang jelas dan kurang mengibaratkan identitas program.
## Soal 4
Pada problematika soal 4, kita diperlukan untuk membuat sebuah shell script file yang nantinya akan men-decrypt dan men-encrypt system log yang ada pada "/var/log/syslog". Nantinya dari file log_encrypt.sh, akan dibuat sebuah cron jobs melalu crontab yang diset otomasinya untuk jalan setiap 2 jam.

Berikut merupakan script yang digunakan untuk melakukan encryption
```
#! /bin/bash

# Setting Up Cron
# Set user's home cron to command path
    cd /home/zeonkun/SISOP/praktikum1/sisop-praktikum-modul-1-2023-am-c05/soal4
# Creates a new crontab
#   crontab -e
# Insert the command and the shell
#   0 */2 * * * /bin/bash /home/zeonkun/SISOP/praktikum1/sisop-praktikum-modul-1-2023-am-c05/soal4/log

# Setting up the SYSLOG path on logPath variable
logPath="/var/log/syslog"

# Alphabets that will be encrypted or ciphered
cypherArtifacts="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

# Encrypt Function, further used in tr command
function encrypt () {
    # Local declaration
    local agentOfEncryption=$1

    # BRUTE FORCE -- DELETE AFTER 10:00 AM
    agentOfEncryption=$(echo $agentOfEncryption | cut -c2)
    
    local temp=""

    # Separates Upper and Lower cases
    upperBound="$(echo ${cypherArtifacts:26:51})"
    lowerBound="$(echo ${cypherArtifacts:0:26})"

    # Get the range for each case using the substring concept {string:startingArtifact:listOfArtifacts}
    temp+="${lowerBound:agentOfEncryption%26:26}${lowerBound:0:agentOfEncryption%26}"
    temp+="${upperBound:agentOfEncryption%26:26}${upperBound:0:agentOfEncryption%26}"
    echo $temp
}

# Implement encrypt function on logPath inner text
function ThyFullEncryptionPack () {
    local filename="$(date +"%H:%M %d:%m:%Y").txt"
    echo -n "$(cat $logPath | tr [$(echo $cypherArtifacts)] [$(encrypt $(date +"%H"))] >> "$filename")"
}

# Fully Encrypt the SYSLOG & Backup the data to a txt file (with TIME FORMAT)
echo -n "$(ThyFullEncryptionPack)"
```
Adapun salah satu hasil dari encyption tersebut adalah sebagai berikut
```
Vja  7 09:39:01 UJYCXY-2JP39TMY abhbuxpm: abhbuxpm'b paxdyrm lqjwpnm cx 110
Vja  7 09:39:01 UJYCXY-2JP39TMY abhbuxpm: abhbuxpm'b dbnarm lqjwpnm cx 104
Vja  7 09:39:01 UJYCXY-2JP39TMY abhbuxpm: [xarprw bxocfjan="abhbuxpm" bfEnabrxw="8.2001.0" g-yrm="1978" g-rwox="qccyb://fff.abhbuxp.lxv"] bcjac 
```
Adapun untuk script decryption adalah sebagai berikut
```
#! /bin/bash

# truncate -s 0 /var/log/syslog

# Set the file to the recent log record & get the hour
filePath="$(ls | awk '/.txt/' | tail -n 1)"
getHour=$(ls | awk '/.txt/' | sort -t: -k1 | tail -n 1 | cut -d':' -f1)

# Alphabets that will be decrypted
cypherArtifacts="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

# Decrypt Function, further used in tr command
function decrypt () {
    # Local declaration
    local agentOfDecryption=$1

    # BRUTE FORCE -- DELETE AFTER 10:00 AM
    agentOfDecryption=$(echo $agentOfDecryption | cut -c2)

    local temp=""

    # Separates Upper and Lower cases
    upperBound="$(echo ${cypherArtifacts:26:51})"
    lowerBound="$(echo ${cypherArtifacts:0:26})"

    # Get the range for each case using the substring concept {string:starting:population}
    temp+="${lowerBound:agentOfDecryption%26:26}${lowerBound:0:agentOfDecryption%26}"
    temp+="${upperBound:agentOfDecryption%26:26}${upperBound:0:agentOfDecryption%26}"
    echo $temp
}

# Implement decrypt function on the encrypted file
function ThyFullDecryptionPack () {
    echo -n "$(cat "$filePath" | tr [$(decrypt $getHour)] [$(echo $cypherArtifacts)])"
}

# Call decrypt pack
echo -n "$(ThyFullDecryptionPack)"
```
dengan hasil decrypt seperti berikut
```
Mar  7 09:39:01 LAPTOP-2AG39KDP rsyslogd: rsyslogd's groupid changed to 110
Mar  7 09:39:01 LAPTOP-2AG39KDP rsyslogd: rsyslogd's userid changed to 104
Mar  7 09:39:01 LAPTOP-2AG39KDP rsyslogd: [origin software="rsyslogd" swVersion="8.2001.0" x-pid="1978" x-info="https://www.rsyslog.com"] start
```
**Kendala :** Pertama kali mencoba menggunakan ascii, tapi ternyata ada command tr. Kemudian ada kendala di crontab juga dimana ternyata harus dilakukan cd terlebih dahulu dari home user.
